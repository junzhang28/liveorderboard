package com.silverbarsmarketplace.model;

import java.math.BigDecimal;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.silverbarsmarketplace.util.OrderType;

/**
 * Used to represent order input from client.
 * 
 * @author Jun Zhang
 *
 */
public class OrderInput {

	@NotNull(message = "User Id is required and must be a positive integer.")
	@Positive
	private long userId;

	@NotNull(message = "Order quantity is required and must be a positive number.")
	@Positive
	private BigDecimal orderQuantity;

	@NotNull(message = "Price per kg is required and must be a positive number.")
	@Positive
	private int pricePerKg;

	@NotNull(message = "Order type is required and must be either 'BUY' or 'SELL'.")
	private OrderType orderType;

	@JsonCreator
	public OrderInput(@JsonProperty("userId") long userId, @JsonProperty("orderQuantity") BigDecimal orderQuantity,
			@JsonProperty("pricePerKg") int pricePerKg, @JsonProperty("orderType") OrderType orderType) {
		this.userId = userId;
		this.orderQuantity = orderQuantity;
		this.pricePerKg = pricePerKg;
		this.orderType = orderType;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public BigDecimal getOrderQuantity() {
		return orderQuantity;
	}

	public void setOrderQuantity(BigDecimal orderQuantity) {
		this.orderQuantity = orderQuantity;
	}

	public int getPricePerKg() {
		return pricePerKg;
	}

	public void setPricePerKg(int pricePerKg) {
		this.pricePerKg = pricePerKg;
	}

	public OrderType getOrderType() {
		return orderType;
	}

	public void setOrderType(OrderType orderType) {
		this.orderType = orderType;
	}
}
