package com.silverbarsmarketplace.model;

import java.math.BigDecimal;

import com.silverbarsmarketplace.util.OrderType;

/**
 * Used to represent a live order.
 * 
 * @author Jun Zhang
 *
 */
public class Order {

	private long orderId;
	private long userId;
	private BigDecimal orderQuantity;
	private int pricePerKg;
	private OrderType orderType;

	public Order(long orderId, long userId, BigDecimal orderQuantity, int pricePerKg, OrderType orderType) {
		this.orderId = orderId;
		this.userId = userId;
		this.orderQuantity = orderQuantity;
		this.pricePerKg = pricePerKg;
		this.orderType = orderType;
	}

	public long getOrderId() {
		return orderId;
	}

	public void setOrderId(long orderId) {
		this.orderId = orderId;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public BigDecimal getOrderQuantity() {
		return orderQuantity;
	}

	public void setOrderQuantity(BigDecimal orderQuantity) {
		this.orderQuantity = orderQuantity;
	}

	public int getPricePerKg() {
		return pricePerKg;
	}

	public void setPricePerKg(int pricePerKg) {
		this.pricePerKg = pricePerKg;
	}

	public OrderType getOrderType() {
		return orderType;
	}

	public void setOrderType(OrderType orderType) {
		this.orderType = orderType;
	}
}