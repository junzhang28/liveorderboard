package com.silverbarsmarketplace.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.silverbarsmarketplace.model.Order;
import com.silverbarsmarketplace.model.OrderInput;
import com.silverbarsmarketplace.service.LiveOrderBoardService;

/**
 * Used to receive all requests from the client then call service class to
 * handle the requests. It returns the response to client once receives results
 * from the service class(es).
 * 
 * @author Jun Zhang
 *
 */

@RestController
@RequestMapping("/liveorderboard")
public class LiveOrderBoardController {

	@Autowired
	private LiveOrderBoardService liveOrderBoardService;

	@GetMapping("")
	public ResponseEntity<String> greet() {
		return ResponseEntity.status(HttpStatus.OK).body("Welcome to Live Order Board!");
	}

	@GetMapping("/version")
	public ResponseEntity<String> getVersion() {
		return ResponseEntity.status(HttpStatus.OK).body("1.0.0");
	}

	@PostMapping("/registerLiveOrder")
	public ResponseEntity<String> registerLiveOrder(@RequestBody @Valid OrderInput orderInput) {
		Optional<Long> orderIdRegistered = liveOrderBoardService.registerLiveOrder(orderInput);

		if (orderIdRegistered.isPresent()) {
			return ResponseEntity.status(HttpStatus.CREATED)
					.body("Order registered successfully. Order Id: " + orderIdRegistered.get());
		}

		return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body("Order NOT registered.");
	}

	@DeleteMapping("/cancelLiveOrder/{orderId}")
	public ResponseEntity<String> cancelLiveOrderWithOrderId(@PathVariable String orderId) {
		if (!liveOrderBoardService.isOrderRegistered(Long.valueOf(orderId))) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Order " + orderId + " NOT found.");
		}

		Optional<Long> orderIdDeleted = liveOrderBoardService.cancelLiveOrderWithOrderId(Long.valueOf(orderId));

		if (orderIdDeleted.isPresent()) {
			return ResponseEntity.status(HttpStatus.OK)
					.body("Order " + orderIdDeleted.get() + " cancelled successfully.");
		}
		return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body("Order " + orderId + " NOT cancelled.");
	}

	@DeleteMapping("/cancelLiveOrder")
	public ResponseEntity<String> cancelLiveOrderWithOrderInput(@RequestBody @Valid OrderInput orderInput) {
		Optional<Long> orderId = liveOrderBoardService.cancelLiveOrderWithOrderInput(orderInput);

		if (orderId.isPresent()) {
			return ResponseEntity.status(HttpStatus.OK).body("Order " + orderId + " cancelled successfully.");
		}
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Order NOT found.");
	}

	@RequestMapping("/liveOrders")
	public List<Order> getLiveOrders() {
		return liveOrderBoardService.getLiveOrders();
	}

	@GetMapping("/buyOrdersSummary")
	public List<String> getBuyOrdersSummary() {
		return liveOrderBoardService.getBuyOrdersSummary();
	}

	@GetMapping("/sellOrdersSummary")
	public List<String> getSellOrdersSummary() {
		return liveOrderBoardService.getSellOrdersSummary();
	}
}
