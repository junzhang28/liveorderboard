package com.silverbarsmarketplace;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Executable main class.
 * 
 * @author Jun Zhang
 *
 */

@SpringBootApplication
public class LiveOrderBoardApp {

	public static void main(String[] args) {
		SpringApplication.run(LiveOrderBoardApp.class, args);

	}

}
