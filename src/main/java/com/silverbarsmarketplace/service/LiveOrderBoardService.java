package com.silverbarsmarketplace.service;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.silverbarsmarketplace.model.Order;
import com.silverbarsmarketplace.model.OrderInput;
import com.silverbarsmarketplace.util.OrderType;

/**
 * Used to handle the requests from the controller and return response to the
 * controller.
 * 
 * @author Jun Zhang
 *
 */

@Service
public class LiveOrderBoardService {

	/*
	 * Use AtomicLong rather than long to ensure the value is synchronized between
	 * threads.
	 */
	private AtomicLong nextOrderId = new AtomicLong(1);

	/*
	 * Use ConcurrentHashMap rather than HashMap to ensure it is synchronized
	 * between threads.
	 */
	private Map<Long, Order> liveOrdersMap = new ConcurrentHashMap<>();

	public Optional<Long> registerLiveOrder(OrderInput orderInput) {
		long orderId = nextOrderId.getAndIncrement();
		Order registeredOrder = liveOrdersMap.put(orderId, new Order(orderId, orderInput.getUserId(),
				orderInput.getOrderQuantity(), orderInput.getPricePerKg(), orderInput.getOrderType()));

		if (registeredOrder == null) {
			return Optional.of(orderId);
		}
		return Optional.empty();
	}

	public List<String> getSellOrdersSummary() {
		Map<Integer, BigDecimal> tempResultMap = liveOrdersMap.values().stream()
				.sorted(Comparator.comparing(Order::getPricePerKg))
				.filter(order -> order.getOrderType().equals(OrderType.SELL))
				.collect(Collectors.toMap(Order::getPricePerKg, Order::getOrderQuantity,
						(oldValue, newValue) -> oldValue.add(newValue), LinkedHashMap::new));

		return tempResultMap.entrySet().stream().map(entry -> entry.getValue() + "kg for £" + entry.getKey())
				.collect(Collectors.toList());
	}

	public List<String> getBuyOrdersSummary() {
		Map<Integer, BigDecimal> tempResultMap = liveOrdersMap.values().stream()
				.sorted(Comparator.comparing(Order::getPricePerKg).reversed())
				.filter(order -> order.getOrderType().equals(OrderType.BUY))
				.collect(Collectors.toMap(Order::getPricePerKg, Order::getOrderQuantity,
						(oldValue, newValue) -> oldValue.add(newValue), LinkedHashMap::new));

		return tempResultMap.entrySet().stream().map(entry -> entry.getValue() + "kg for £" + entry.getKey())
				.collect(Collectors.toList());
	}

	public boolean isOrderRegistered(long orderId) {
		if (liveOrdersMap.containsKey(orderId)) {
			return true;
		}
		return false;
	}

	public List<Order> getLiveOrders() {
		return liveOrdersMap.values().stream().collect(Collectors.toList());
	}

	public Optional<Long> cancelLiveOrderWithOrderId(long orderId) {
		if (!isOrderRegistered(orderId)) {
			return Optional.empty();
		}
		return Optional.of(liveOrdersMap.remove(orderId).getOrderId());
	}

	public Optional<Long> cancelLiveOrderWithOrderInput(OrderInput orderInput) {
		for (Map.Entry<Long, Order> entry : liveOrdersMap.entrySet()) {
			Order order = entry.getValue();
			if (order.getUserId() == orderInput.getUserId()
					&& order.getOrderQuantity().equals(orderInput.getOrderQuantity())
					&& order.getPricePerKg() == orderInput.getPricePerKg()
					&& order.getOrderType().equals(orderInput.getOrderType())) {
				long orderId = entry.getKey();
				liveOrdersMap.remove(orderId);
				return Optional.of(orderId);
			}
		}
		return Optional.empty();
	}
}
