package com.silverbarsmarketplace.util;

/**
 * Used to represent order type "BUY" and "SELL".
 * 
 * @author Jun Zhang
 *
 */
public enum OrderType {
	BUY, SELL;
}
