package com.silverbarsmarketplace.controller;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.silverbarsmarketplace.model.OrderInput;
import com.silverbarsmarketplace.util.OrderType;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class LiveOrderBoardControllerTest {

	@Autowired
	private MockMvc mockMvc;

	private JacksonTester<OrderInput> jsonCreator;

	@Test
	public void shouldReturnDefaultMessage() throws Exception {
		this.mockMvc.perform(get("/liveorderboard/version")).andDo(print()).andExpect(status().isOk())
				.andExpect(content().string(containsString("1.0.0")));
	}

	@Test
	public void registerLiveOrderWithNullObject() throws Exception {
		this.mockMvc.perform(post("/liveorderboard/registerLiveOrder").contentType(MediaType.APPLICATION_JSON))
				.andDo(print()).andExpect(status().isBadRequest());
	}

	@Test
	public void registerLiveOrderWithCorrectObjectInput() throws Exception {
		OrderInput orderInput = new OrderInput(1, new BigDecimal("3.5"), 306, OrderType.SELL);
		this.mockMvc.perform(post("/liveorderboard/registerLiveOrder").contentType(MediaType.APPLICATION_JSON)
				.content(asJsonString(orderInput))).andDo(print()).andExpect(status().isCreated());
	}

	@Test
	public void cancelLiveOrderWithNonExistedOrderId() throws Exception {
		this.mockMvc.perform(delete("/liveorderboard/cancelLiveOrder/1")).andDo(print())
				.andExpect(status().isNotFound());
	}

	@Test
	public void cancelLiveOrderWithRegisteredOrderId() throws Exception {
		OrderInput orderInput = new OrderInput(1, new BigDecimal("3.5"), 306, OrderType.SELL);
		this.mockMvc.perform(post("/liveorderboard/registerLiveOrder").contentType(MediaType.APPLICATION_JSON)
				.content(asJsonString(orderInput))).andDo(print()).andExpect(status().isCreated());
		this.mockMvc.perform(delete("/liveorderboard/cancelLiveOrder/1")).andDo(print()).andExpect(status().isOk());
	}

	@Test
	public void cancelLiveOrderWithNonRegisteredOrderInput() throws Exception {
		OrderInput orderInput = new OrderInput(1, new BigDecimal("3.5"), 306, OrderType.SELL);
		this.mockMvc.perform(post("/liveorderboard/registerLiveOrder").contentType(MediaType.APPLICATION_JSON)
				.content(asJsonString(orderInput))).andDo(print()).andExpect(status().isCreated());
		OrderInput nonRegisteredOrderInput = new OrderInput(1, new BigDecimal("1.5"), 306, OrderType.SELL);
		this.mockMvc
				.perform(delete("/liveorderboard/cancelLiveOrder/1").contentType(MediaType.APPLICATION_JSON)
						.content(asJsonString(nonRegisteredOrderInput)))
				.andDo(print()).andExpect(status().isNotFound());
	}

	@Test
	public void shouldReturnBuyOrderSummary() throws Exception {
		this.mockMvc.perform(get("/liveorderboard/buyOrdersSummary")).andDo(print()).andExpect(status().isOk());
	}

	@Test
	public void shouldReturnSellOrderSummary() throws Exception {
		this.mockMvc.perform(get("/liveorderboard/sellOrdersSummary")).andDo(print()).andExpect(status().isOk());
	}

	public static String asJsonString(final Object obj) {
		try {
			return new ObjectMapper().writeValueAsString(obj);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
