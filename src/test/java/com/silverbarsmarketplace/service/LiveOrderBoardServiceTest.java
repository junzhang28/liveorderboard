package com.silverbarsmarketplace.service;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsIterableContainingInOrder.contains;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.silverbarsmarketplace.model.OrderInput;
import com.silverbarsmarketplace.util.OrderType;

@RunWith(SpringRunner.class)
@SpringBootTest
public class LiveOrderBoardServiceTest {

	private LiveOrderBoardService liveOrderBoardService;

	@Before
	public void init() {
		liveOrderBoardService = new LiveOrderBoardService();
	}

	@Test(expected = NullPointerException.class)
	public void NullObjectRegisterationThrowsException() {
		liveOrderBoardService.registerLiveOrder(null);
	}

	@Test
	public void liveOrderCanBeRegisteredWithCorrectInput() {
		Optional<Long> orderId = liveOrderBoardService
				.registerLiveOrder(new OrderInput(1, new BigDecimal("3.5"), 306, OrderType.SELL));
		assertEquals(Optional.of(1L), orderId);
	}

	@Test
	public void nonRegisteredOrderCanNotBeCancelledWithOrderId() {
		Optional<Long> orderId = liveOrderBoardService.cancelLiveOrderWithOrderId(1);
		assertEquals(Optional.empty(), orderId);
	}

	@Test
	public void registeredOrderCanBeCancelledWithOrderId() {
		liveOrderBoardService.registerLiveOrder(new OrderInput(1, new BigDecimal("3.5"), 306, OrderType.SELL));
		Optional<Long> orderId = liveOrderBoardService.cancelLiveOrderWithOrderId(1);
		assertEquals(Optional.of(1L), orderId);
	}

	@Test
	public void nonRegisteredOrderCanNotBeCancelledWithOrderInput() {
		Optional<Long> orderId = liveOrderBoardService
				.cancelLiveOrderWithOrderInput(new OrderInput(1, new BigDecimal("3.5"), 306, OrderType.SELL));
		assertEquals(Optional.empty(), orderId);
	}

	@Test
	public void registeredOrderCanBeCancelledWithOrderInput() {
		OrderInput orderInput1 = new OrderInput(1, new BigDecimal("3.5"), 306, OrderType.SELL);
		OrderInput orderInput2 = new OrderInput(1, new BigDecimal("1.2"), 310, OrderType.SELL);
		OrderInput orderInput3 = new OrderInput(1, new BigDecimal("1.5"), 307, OrderType.SELL);
		liveOrderBoardService.registerLiveOrder(orderInput1);
		liveOrderBoardService.registerLiveOrder(orderInput2);
		liveOrderBoardService.registerLiveOrder(orderInput3);
		Optional<Long> orderId = liveOrderBoardService.cancelLiveOrderWithOrderInput(orderInput2);
		assertEquals(Optional.of(2L), orderId);
	}

	@Test
	public void sellOrdersSummaryWhenNoOrderRegistered() {
		List<String> summary = liveOrderBoardService.getSellOrdersSummary();
		assertEquals(0, summary.size());
	}

	@Test
	public void sellOrdersSummaryWhenFourOrdersRegistered() {
		liveOrderBoardService.registerLiveOrder(new OrderInput(1, new BigDecimal("3.5"), 306, OrderType.SELL));
		liveOrderBoardService.registerLiveOrder(new OrderInput(2, new BigDecimal("1.2"), 310, OrderType.SELL));
		liveOrderBoardService.registerLiveOrder(new OrderInput(3, new BigDecimal("1.5"), 307, OrderType.SELL));
		liveOrderBoardService.registerLiveOrder(new OrderInput(4, new BigDecimal("2.0"), 306, OrderType.SELL));
		List<String> actualSummary = liveOrderBoardService.getSellOrdersSummary();

		// Ensure Correct order
		assertThat(actualSummary, contains("5.5kg for £306", "1.5kg for £307", "1.2kg for £310"));
	}

	@Test
	public void buyOrdersSummaryWhenNoOrderRegistered() {
		List<String> summary = liveOrderBoardService.getSellOrdersSummary();
		assertEquals(0, summary.size());
	}

	@Test
	public void buyOrdersSummaryWhenFourOrdersRegistered() {
		liveOrderBoardService.registerLiveOrder(new OrderInput(1, new BigDecimal("3.5"), 306, OrderType.BUY));
		liveOrderBoardService.registerLiveOrder(new OrderInput(2, new BigDecimal("1.2"), 310, OrderType.BUY));
		liveOrderBoardService.registerLiveOrder(new OrderInput(3, new BigDecimal("1.5"), 307, OrderType.BUY));
		liveOrderBoardService.registerLiveOrder(new OrderInput(4, new BigDecimal("2.0"), 306, OrderType.BUY));
		List<String> actualSummary = liveOrderBoardService.getBuyOrdersSummary();

		// Ensure Correct order
		assertThat(actualSummary, contains("1.2kg for £310", "1.5kg for £307", "5.5kg for £306"));
	}

	@Test
	public void isOrderRegisteredReturnsFalseWhenOrderNoRegistered() {
		assertFalse(liveOrderBoardService.isOrderRegistered(1));
	}

	@Test
	public void isOrderRegisteredReturnsTrueWhenOrderRegistered() {
		liveOrderBoardService.registerLiveOrder(new OrderInput(1, new BigDecimal("3.5"), 306, OrderType.BUY));
		liveOrderBoardService.registerLiveOrder(new OrderInput(2, new BigDecimal("1.2"), 310, OrderType.BUY));
		assertTrue(liveOrderBoardService.isOrderRegistered(1));
	}
}
