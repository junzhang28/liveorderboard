# Solution to Live Order Board

This project provides the solution to the interview exercise - Live Order Board. To make sure the solution can be packaged and shipped to the UI team, the solution implemented as a web service with Java and Spring Boot and exposed with REST APIs. 

## Getting Started

Check out the source code and use Maven to build the project: 
```
mvn package
```
or build the project without tests:
```
mvn package -DskipTests
```
Execute the built jar file: 
```
java -jar target/LiveOrderBoard-1.0.0.jar
```
Check the service is up and running:
```
curl http://localhost:8080/liveorderboard
Welcome to Live Order Board!
```
To access other resources:
```
curl http://localhost:8080/liveorderboard/{resource URL}
```
## REST Resources Reference
------------------
###1. Register an order.

**URL:** /registerLiveOrder

**Method:** POST

**Request Headers:**
```
Content-Type – application/json
```
**Data Parameters:**
```
{
	"userId": [string],
	"orderQuantity": [positive number],
	"pricePerKg": [positive integer],
	"orderType": "BUY" or "SELL"
}
```
Example:
```
{
	"userId": "1",
	"orderQuantity": 3.5,
	"pricePerKg": 306,
	"orderType": SELL”
}
```
**Success Response:**
```
Status Code: 201 Created
Content: Order registered successfully. Order Id: [generated order id] 
```
Example:
```
Status Code: 201 Created
Content: Order registered successfully. Order Id: 1
```

**Error Response:**
```
Status Code: 422 Unprocessable Entity
Content: Order NOT registered. 
```
------------------
###2. Cancel a registered order with order id.

**URL:** /cancelLiveOrder/{orderId}

**Method:** DELETE

**URL Parameters:**
```
Required:
orderId - [positive integer]
```
URL Example:
```
/cancelLiveOrder/1
```

**Success Response:**
```
Status Code: 200 OK
Content: Order [order id] cancelled successfully. 
```
Example:
```
Status Code: 200 OK
Content: Order 1 cancelled successfully.
```

**Error Response:**
```
Status Code: 422 Unprocessable Entity
Content: Order [order id] NOT cancelled. 
```
Example:
```
Status Code: 422 Unprocessable Entity
Content: Order 1 NOT cancelled. 
```
------------------
###3. Cancel a registered order with order details.

**URL:** /cancelLiveOrder

**Method:** DELETE

**Request Headers:**
```
Content-Type – application/json
```

**Data Parameters:**
```
{
	"userId": [string],
	"orderQuantity": [positive number],
	"pricePerKg": [positive integer],
	"orderType": "BUY" or "SELL"
}
```
Example:
```
{
	"userId": "1",
	"orderQuantity": 3.5,
	"pricePerKg": 306,
	"orderType": SELL”
}
```

**Success Response:**
```
Status Code: 200 OK
Content: Order [order id] cancelled successfully. 
```
Example:
```
Status Code: 200 OK
Content: Order 1 cancelled successfully.
```

**Error Response:**
```
Status Code: 422 Unprocessable Entity
Content: Order [order id] NOT cancelled. 
```
Example:
```
Status Code: 422 Unprocessable Entity
Content: Order 1 NOT cancelled. 
```
------------------
###4. Get summary information of live buy orders.

**URL:** /buyOrdersSummary

**Method:** GET

**Success Response:**
```
Status Code: 200 OK
Content:
[
	"[order quantities 1] for [price 1]",
	"[order quantities 2] for [price 2]",
	"[order quantities 3] for [price 3]",
	...
]
```
Example:
```
Status Code: 200 OK
Content: 
[
    "1.2kg for £310",
    "1.5kg for £307",
    "5.5kg for £306"
]
```
------------------
###5. Get summary information of live sell orders.

**URL:** /sellOrdersSummary

**Method:** GET

**Success Response:**
```
Status Code: 200 OK
Content:
[
	"[order quantities 1] for [price 1]",
	"[order quantities 2] for [price 2]",
	"[order quantities 3] for [price 3]",
	...
]
```
Example:
```
Status Code: 200 OK
Content: 
[
    "5.5kg for £306",
	"1.5kg for £307",
	"1.2kg for £310"
]
```


